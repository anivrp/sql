AND

SELECT * FROM Customers
WHERE Country='Germany' AND City='Berlin';


OR 

SELECT * FROM Customers
WHERE City='Berlin' OR City='München';

NOT

SELECT * FROM Customers
WHERE NOT Country='Germany';


Combining AND, OR and NOT

SELECT * FROM Customers
WHERE Country='Germany' AND (City='Berlin' OR City='München');